﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AuthUserManager
{
    [Serializable]
    public class AppSetting
    {
        public string ConnectionString { get; set; }
    }

}
