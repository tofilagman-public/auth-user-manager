﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AuthUserManager.Services
{
    public class AspNetUserPastPasswordsService : IAspNetUserPastPasswordsService
    {
        private ApplicationDbContext DbContext;

        public AspNetUserPastPasswordsService(ApplicationDbContext dbContext)
        {
            this.DbContext = dbContext;
        }

        public void UpdatePastPasswordTimeStamp(string userId)
        {
            var user = this.DbContext.aspNetUserPastPasswords.Last(x=> x.UserId == userId);
            user.Created = DateTime.Now;
            this.DbContext.SaveChanges();
        } 
    }

    public interface IAspNetUserPastPasswordsService
    {
        void UpdatePastPasswordTimeStamp(string userId);
    }
}
