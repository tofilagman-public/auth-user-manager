﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AuthUserManager.Models
{
  public  class AspNetUserPastPasswords
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string PasswordHash { get; set; }
        public DateTime Created { get; set; }
    }
}
