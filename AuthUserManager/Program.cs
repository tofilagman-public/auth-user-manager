﻿using AuthUserManager.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading;

namespace AuthUserManager
{
    class Program
    {
        private static bool Exited = false;
        private const string CONFIG = "appsettings.json";
        private static AppSetting AppSetting = new AppSetting();
        private static IServiceProvider ServiceProvider;

        private static readonly string nFile = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetExecutingAssembly().Location), CONFIG);

        static void Main(string[] args)
        {
            try
            {
                Console.CancelKeyPress += (o, e) => Environment.Exit(0);
                Run();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }
        }

        static void Run()
        {
            ReadConfig();
            Connect();
            Setup();
            Transact();
        }

        static void ReadConfig()
        {
            if (File.Exists(nFile))
                AppSetting = JsonConvert.DeserializeObject<AppSetting>(File.ReadAllText(nFile));
            else
            {
                SetupConnection();
            }
        }

        static bool SetupConnection()
        {
            if (AskForConfirmation("Would you like to setup server connection?").Key == ConsoleKey.Y)
            {
                var strBuilder = new SqlConnectionStringBuilder
                {
                    ConnectionString = AppSetting.ConnectionString
                };

                strBuilder.DataSource = AskForText("DataSource", strBuilder.DataSource);

                if (AskForConfirmation("Use Integrated Security?").Key == ConsoleKey.Y)
                {
                    strBuilder.IntegratedSecurity = true;
                }
                else
                {
                    strBuilder.IntegratedSecurity = false;
                    strBuilder.UserID = AskForText("User ID", strBuilder.UserID);
                    strBuilder.Password = AskForText("Password", strBuilder.Password);
                }

                strBuilder.InitialCatalog = AskForText("Database", strBuilder.InitialCatalog);

                AppSetting.ConnectionString = strBuilder.ConnectionString;

                if (!Directory.Exists(Path.GetDirectoryName(nFile)))
                    Directory.CreateDirectory(Path.GetDirectoryName(nFile));
                File.WriteAllText(nFile, JsonConvert.SerializeObject(AppSetting));

                return true;
            }
            return false;
        }

        static void Connect()
        {
            try
            {
                if (string.IsNullOrEmpty(AppSetting.ConnectionString))
                    throw new Exception("Connection String has not yet initialized. Exiting");

                Console.WriteLine($"Connecting to server: { AppSetting.ConnectionString }");
                using (var con = new SqlConnection(AppSetting.ConnectionString))
                {
                    con.Open();
                    Console.WriteLine("Connected!");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                if (SetupConnection())
                    Run();
            }
        }

        static void Setup()
        {
            IServiceCollection services = new ServiceCollection();
            Startup startup = new Startup();
            startup.ConfigureServices(AppSetting.ConnectionString, services);

            ServiceProvider = services.BuildServiceProvider();
        }

        static int Transact()
        {
            var cancel = CancellationToken.None;
            while (true)
            {
                var conf = AskForConfirmation("[A] Add User, [U] Update User, [D] Remove User, [L] List Users, [S] Search for User, [C] Count, [O] Others, [Q] Quit", false);

                if (conf.Key == ConsoleKey.Q)
                    return 0;

                Console.WriteLine();
                Console.Clear();

                using (var scope = ServiceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
                {
                    var userManager = scope.ServiceProvider.GetRequiredService<UserManager<AppUser>>();
                    switch (conf.Key)
                    {
                        case ConsoleKey.C:
                            Console.WriteLine("Counting Please wait ...");
                            var count = userManager.Users.Count();
                            Console.WriteLine($"Current User(s) Count: {count}");
                            break;
                        case ConsoleKey.L:
                            Console.WriteLine("Getting List Please wait ...");
                            var nlst = userManager.Users;
                            Console.WriteLine(nlst.ToStringTable(new[] { "Id", "Name", "Email" }, a => a.Id, a => a.UserName, a => a.Email));
                            break;
                        case ConsoleKey.S:
                            var jm = AskForText("Enter Username or Email").ToLower();
                            Console.WriteLine("Querying Please wait ...");
                            var k = userManager.Users.Where(x => x.Email.ToLower().Contains(jm) || x.UserName.ToLower().Contains(jm));
                            Console.WriteLine(k.ToStringTable(new[] { "Id", "Name", "Email" }, a => a.Id, a => a.UserName, a => a.Email));
                            break;
                        case ConsoleKey.A:
                            var newUser = new AppUser
                            {
                                EmailConfirmed = true,
                                Email = AskForText("Email"),
                                LockoutEnabled = true,
                                UserName = AskForText("UserName"),
                            };
                            //check if exists
                            if (userManager.FindByNameAsync(newUser.UserName).GetAwaiter().GetResult() != null || userManager.FindByEmailAsync(newUser.Email).GetAwaiter().GetResult() != null)
                                Console.WriteLine("User already exists");
                            else
                            {
                                var nkd = userManager.CreateAsync(newUser, AskForText("Password")).GetAwaiter().GetResult();
                                if (nkd.Succeeded)
                                    Console.WriteLine("User successfully registered");
                                else
                                    Console.WriteLine(nkd.Errors.First().Description);
                            }
                            break;
                        case ConsoleKey.U:
                            var username = AskForText("User name");
                            var nk = userManager.FindByNameAsync(username).GetAwaiter().GetResult();
                            if (nk == null)
                                Console.WriteLine("User not exists");
                            else
                            {
                                nk.UserName = AskForText("New Username", nk.UserName);
                                nk.Email = AskForText("New Email", nk.Email);
                                var nPass = AskForText("New Password", "********");
                                var gnd = default(IdentityResult);
                                if (nPass != "********")
                                {
                                    var token = userManager.GeneratePasswordResetTokenAsync(nk).GetAwaiter().GetResult();
                                    gnd = userManager.ResetPasswordAsync(nk, token, nPass).GetAwaiter().GetResult();

                                    if (!gnd.Succeeded)
                                    {
                                        Console.WriteLine(gnd.Errors.First().Description);
                                        break;
                                    }

                                    //hack password history
                                    var passwordService = scope.ServiceProvider.GetRequiredService<IAspNetUserPastPasswordsService>();
                                    passwordService.UpdatePastPasswordTimeStamp(nk.Id);
                                }
                                gnd = userManager.UpdateAsync(nk).GetAwaiter().GetResult();
                                if (gnd.Succeeded)
                                {
                                    Console.WriteLine("User successfully updated");
                                }
                                else
                                    Console.WriteLine(gnd.Errors.First().Description);
                            }
                            break;
                        case ConsoleKey.D:
                            var nema = AskForText("User name");
                            var gd = userManager.FindByNameAsync(nema).GetAwaiter().GetResult();
                            if (gd != null)
                            {
                                var fdg = userManager.DeleteAsync(gd).GetAwaiter().GetResult();
                                if (fdg.Succeeded)
                                    Console.WriteLine("User successfully deleted");
                                else
                                    Console.WriteLine(fdg.Errors.First().Description);
                            }
                            else
                                Console.WriteLine("User not exists");
                            break;
                        case ConsoleKey.O:
                            OtherOptions();
                            break;
                        default:
                            continue;
                    }
                }
                Console.WriteLine("-- EOL --");
                Console.Read();
                Console.Clear();
            }
        }

        static void OtherOptions()
        {
            Console.Clear();
            SetupConnection();
            Connect();
            Setup();
        }

        static ConsoleKeyInfo AskForConfirmation(string Message, bool YesNo = true)
        {
            while (true)
            {
                if (YesNo)
                    Console.Write($"{Message} Y/n:");
                else
                    Console.Write($"{Message}:");
                var d = Console.ReadKey();
                Console.WriteLine();

                if (YesNo)
                {
                    if (d.Key == ConsoleKey.Y || d.Key == ConsoleKey.N)
                    {
                        return d;
                    }
                }
                else
                    return d;
            }
        }

        static string AskForText(string Message, string nValue = null)
        {
            while (true)
            {
                if (nValue != null)
                    Console.Write($"{Message} [{nValue}]:");
                else
                    Console.Write($"{Message}:");
                var d = Console.ReadLine();
                if (!string.IsNullOrWhiteSpace(d))
                    return d;
                else if (string.IsNullOrEmpty(d) && nValue != null)
                    return nValue;

            }
        }
    }
}
